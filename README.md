# What is this?
Factoid is an ORM library for Android Applications. It aims to provide an easy way to interact with SQLite databases on Android.

In short, it allows you to do something as beautiful as this:

```java
void createStudentPretty(String studentName)
{
	Student newStudent = new Student(); 
	newStudent.name = name;
	newStudent.create();
}
```

Compare to the old-fashioned way:

```java
void createStudentUgly(String studentName)
{
	ContentValues values = new ContentValues(); 
	values.put("name", studentName);
	myDatabase.insert("Students", null, values);
}

void createStudentReallyUglyDontEverDoThis(String studentName)
{
	String sql = "INSERT INTO Students (name) " + "VALUES ('" + studentName + "')";
	myDatabase.execSql( sql );
}
```

A lot prettier huh?



# Getting Started
You should really be using a Gradle-based build system, such as [Android Studio](http://developer.android.com/sdk/installing/studio.html). If you are, using factoid is as simple as adding the following line to your _build.gradle_ dependencies:

```javascript
compile 'com.factati:factoid:2.18'
```
The latest version number can be found [here](http://mvnrepository.com/artifact/com.factati/factoid)

# Working with factoid
To work with _factoid_, you should follow 3 steps:

1. Declare your persistent data on domain classes with public fields
2. Tell factoid which classes must become SQLite tables
3. Use the factoid API (aka _**profit!**_)

## Creating Domain Classes
#### Simple Example

```java
import com.factati.factoid.database.orm.*;

@SQLiteTable
public class Student extends AnnotatedTable<Student>{
	
	@SQLiteColumn(notNull = true)
	public String name;

	@SQLiteColumn
	public Long age;

	@SQLiteColumn
	public Boolean isEnrolled;	
	}
}
```

#### One-to-Many Example

```java
import com.factati.factoid.database.orm.*;

@SQLiteTable
public class Student extends AnnotatedTable<Student>{
	
	@SQLiteColumn(notNull = true)
	public String name;

	@SQLiteColumn
	public Long age;

	@SQLiteColumn
	public Boolean isEnrolled;	
	}
}
```

```java
import com.factati.factoid.database.orm.*;

@SQLiteTable
public class Grade extends AnnotatedTable<Grade>{
	
	@SQLiteColumn(references = Student.class)
	public Long studentId;

	@SQLiteColumn
	public String courseName;

	@SQLiteColumn
	public Long gradeValue;
	}
}
```



## _Factoid_ Configuration
First you should extend the Factoid Configuration class:
```java
import com.factati.factoid.config.IFactoidConfig;

public class FactoidExampleConfig implements IFactoidConfig {

	// Increment this value every time the database needs to be dropped and recreated (after an ApplicationUpdate).
	private static final int DATABASE_VERSION = 1;
	
	@Override
	public String getDatabaseName() {
		return "studentExample.db";
	}

	@Override
	public int getDatabaseVersion() {
		return DATABASE_VERSION;
	}
}	
```

Now, somewhere early in your application just tell which is the _factoid_ config class and which domain classes must be persisted:
```java
import com.factati.factoid.config.FactoidConfig;
import com.factati.factoid.database.DatabaseManager;

public class MainActivity {
    static {
        FactoidConfig.setConfig(new FactoidExampleConfig ());
        DatabaseManager.getInstance().addTable(Student.class);
        DatabaseManager.getInstance().addTable(Grade.class);      
   }
```
That's it!



## _Factoid_ Use

#### Create
```java
public Long createStudent(String studentName, Long studentAge, Boolean enrolled)
{
		Student student = new Student();
		student.name = studentName;
		student.age = studentAge;
		student.isEnrolled = enrolled;

		student.create();
		return student._id; // Id of newly created record
}
```

#### Retrieve
```java
public Student retrieveStudent(Long studentId)
{
		return new Student().get(studentId);
}
```

#### Update
```java
public void updateStudent(Long studentId, String newAge, Boolean newEnrollment)
{
		Student student = new Student().get(studentId);
		student.age = newAge;
		student.isEnrolled = newEnrollment;

		student.update();
}
```

#### Delete
```java
public void deleteStudent(Long studentId)
{
		Student student = new Student().get(studentId);
		student.delete();
}

public void deleteAllStudents()
{
	new Student().deleteAll();
}
```

#### Listing and Support Queries
```java
public List<Student> listAllStudents()
{
		return new Student().listAll();
}

public List<Student> listStudentsByAgeAndEnrollment(Long studentAge, Boolean studentEnrollment)
{
		return new Student().getList("age=? AND isEnrolled=?", new String[]{studentAge.toString(), studentEnrollment.toString()});
}

public Student getFirstStudentByName(String studentName)
{
		return new Student().getOne("name=?", new String[]{studentName});
}

public Long getYoungStudentCount(Long maxAge)
{
		return new Student().getCount("age<?", new String[]{maxAge.toString()});
}

public List<Student> listStudentsByEnrollmentOrderedByAge(Boolean studentEnrollment)
{
	return new Student().getSortedList("isEnrolled=?", new String[]{studentEnrollment.toString()}, "age desc");
}
```

