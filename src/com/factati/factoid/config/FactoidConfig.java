package com.factati.factoid.config;

public class FactoidConfig {

	private static IFactoidConfig config;

	public static IFactoidConfig getConfig() {
		return config;
	}

	public static void setConfig(IFactoidConfig config) {
		FactoidConfig.config = config;
	}
	
}
