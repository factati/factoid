package com.factati.factoid.config;

import java.util.ArrayList;
import java.util.Map;

public interface IFactoidConfig {
	public String getDatabaseName();
	public int getCurrentDatabaseVersion();
	public int getLastDatabaseVersion();
	public String[] getPreferencesToDeleteOnDatabaseUpgrade();
	public Map<Integer, ArrayList<String>> getHashUpgrade();
}
