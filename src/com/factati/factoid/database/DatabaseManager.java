package com.factati.factoid.database;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.factati.factoid.config.FactoidConfig;
import com.factati.factoid.database.orm.IDatabaseModel;

public class DatabaseManager {
	private static final String TAG = "DatabaseManager";
	
	private static String DATABASE_NAME = FactoidConfig.getConfig().getDatabaseName();
	private static int CURRENT_DATABASE_VERSION = FactoidConfig.getConfig().getCurrentDatabaseVersion();
	private static int LAST_DATABASE_VERSION = FactoidConfig.getConfig().getLastDatabaseVersion();
	private static Map<Integer,ArrayList<String>> HASH_UPGRADE = FactoidConfig.getConfig().getHashUpgrade();
	private static String[] PREF_LIST_TO_DELETE_ON_UPGRADE = FactoidConfig.getConfig().getPreferencesToDeleteOnDatabaseUpgrade();
	
	private static final DatabaseManager instance = new DatabaseManager();
	
	private static ArrayList<IDatabaseModel> tablesToCreate = new ArrayList<IDatabaseModel>();
	private static DatabaseHelper mDbHelper;
	private static SQLiteDatabase mWritableDB;



	public static DatabaseManager getInstance() {
		return instance;
	}

	public SQLiteDatabase getDatabase() throws SQLException {
		return mWritableDB;
	}

	public void open(Context context) throws SQLException {
		if (mDbHelper == null)
			mDbHelper = new DatabaseHelper(context);

		mWritableDB = mDbHelper.getWritableDatabase();
	}

	public synchronized void openReadOnly(Context context) throws SQLException {
		if (mDbHelper == null)
			mDbHelper = new DatabaseHelper(context);

		mWritableDB = mDbHelper.getReadableDatabase();
	}

	// If we have a singleton DatabaseHelper, there is no need to close the
	// connection,
	// as it will be closed when the Garbage Collector frees the DatabaseHelper.
	// Additional thoughts at
	// http://touchlabblog.tumblr.com/post/24474750219/single-sqlite-connection
	public void close() {
	}

	public void wipeAllData() {
		mDbHelper.onUpgrade(mWritableDB, CURRENT_DATABASE_VERSION, LAST_DATABASE_VERSION);
	}

	protected void setWritableDatabase(SQLiteDatabase pWritableDB) {
		mWritableDB = pWritableDB;
	}

	private static class DatabaseHelper extends SQLiteOpenHelper {

        Context currentContext;

		DatabaseHelper(Context context) {
			super(context, DATABASE_NAME, null, CURRENT_DATABASE_VERSION);
            currentContext = context;
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			ArrayList<IDatabaseModel> tableList = DatabaseManager.getInstance().getAllTableClasses();

			db.execSQL("PRAGMA foreign_keys=ON;");

			for (IDatabaseModel table : tableList) {
				db.execSQL(table.getCreationScript());
			}

			DatabaseManager.getInstance().setWritableDatabase(db);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			oldVersion = db.getVersion();

			if(HASH_UPGRADE != null) {
				Iterator<Map.Entry<Integer, ArrayList<String>>> entries = HASH_UPGRADE.entrySet().iterator();
				while (entries.hasNext()) {
					Map.Entry<Integer, ArrayList<String>> entry = entries.next();
                    if(entry.getKey() > oldVersion) {
                        for (String sqlQuery : entry.getValue()) {
                            db.execSQL(sqlQuery);
                        }
                    }
				}
			}
		}
	}

	public ArrayList<IDatabaseModel> getAllTableClasses() {
		return tablesToCreate;
	}

	public void addTable(Class<? extends IDatabaseModel> tableClass) {
		try {
			tablesToCreate.add(tableClass.newInstance());
		} catch (Exception ex) {
			ex.printStackTrace();
			Log.e(TAG, "The table " + tableClass + " could not be created");
		}
	}

	public void beginTransaction() {

		if(mWritableDB != null)
			mWritableDB.beginTransaction();
	}

	public void setTransactionSuccessful() {

		if(mWritableDB != null)
			mWritableDB.setTransactionSuccessful();
	}

	public void endTransaction() {

		if(mWritableDB != null)
			mWritableDB.endTransaction();
	}
}
