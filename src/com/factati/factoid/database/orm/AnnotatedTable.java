package com.factati.factoid.database.orm;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.database.Cursor;

import com.factati.factoid.database.DatabaseManager;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public abstract class AnnotatedTable<T> implements IDatabaseModel
{
	@JsonIgnore
	public static final String PARSE_MISSING_MESSAGE = "Não existe tratamento em AnnotatedTable.createOneFromCursor() para o campo %s da tabela %s. Valor a ser tratado: %s";

	@JsonIgnore
	protected final String tableName = this.getClass().getSimpleName();

	@SQLiteColumn(id = true)
	@JsonIgnore
	public Long _id;

	@JsonIgnore
	public String getDropScript()
	{
		return "DROP TABLE IF EXISTS " + tableName;
	}


	@Override
	public int hashCode()
	{
		return _id.intValue();
	}


	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object o)
	{
		if(o == null || this._id == null)
			return false;
		
		AnnotatedTable<T> other = (AnnotatedTable<T>) o;
		return this._id.equals( other._id );
	}


	@JsonIgnore
	public String getCreationScript()
	{
		try
		{
			StringBuilder strBuilder = new StringBuilder();
			strBuilder.append("CREATE TABLE ");
			strBuilder.append(tableName);
			strBuilder.append("( ");

			Field[] fieldArray = this.getClass().getFields();
			ArrayList fieldList = new ArrayList<Field>();

			for(int iterator = 0; iterator < fieldArray.length; iterator++) {
				Field field = fieldArray[iterator];

				if(field.isAnnotationPresent(SQLiteColumn.class))
					fieldList.add(field);
			}

			for(int iterator = 0; iterator < fieldList.size(); iterator++)
			{
				Field field = (Field) fieldList.get(iterator);

				SQLiteColumn annotation = field.getAnnotation(SQLiteColumn.class);
				strBuilder.append(field.getName());
				if(field.getType() == String.class)
					strBuilder.append(" TEXT");
				else if(field.getType() == Double.class)
					strBuilder.append(" REAL");
				else
					strBuilder.append(" INTEGER");

				if(annotation.id())
					strBuilder.append(" PRIMARY KEY AUTOINCREMENT");

				strBuilder.append(annotation.notNull()? " NOT NULL " : " NULL ");

				if(annotation.defaultValue().length() > 0)
				{
					strBuilder.append(" DEFAULT ");
					strBuilder.append(annotation.defaultValue());
				}

				if(annotation.references() != Object.class)
				{
					strBuilder.append(" REFERENCES ");
					strBuilder.append(annotation.references().getSimpleName());
				}

				if(iterator < (fieldList.size() - 1))
					strBuilder.append(",");

			}

			strBuilder.replace(strBuilder.length() - 1, strBuilder.length() - 1, "");
			strBuilder.append(");");

			return strBuilder.toString();
		}
		catch(Exception ex)
		{
			return ex.getMessage();
		}
	}


	private ContentValues getContentValues() throws Exception
	{
		ContentValues contentValues = new ContentValues();
		Field[] fieldArray = this.getClass().getDeclaredFields();
		for(int iterator = 0; iterator < fieldArray.length; iterator++)
		{
			Field field = fieldArray[iterator];
			if(field.isAnnotationPresent(SQLiteColumn.class))
			{
				Object fieldValue = field.get(this);
				if (fieldValue == null)
					contentValues.putNull(field.getName());
				else
					contentValues.put(field.getName(), fieldValue.toString());
			}
		}
		return contentValues;
	}


	@SuppressWarnings("unchecked")
	private T createOneFromCursor(Cursor cursor) throws Exception
	{
		Field[] fieldArray = this.getClass().getFields();
		Object instance = this.getClass().newInstance();
		for(int iterator = 0; iterator < fieldArray.length; iterator++)
		{
			Field field = fieldArray[iterator];
			if(field.isAnnotationPresent(SQLiteColumn.class))
			{

				String stringValue = cursor.getString(cursor.getColumnIndex(field.getName()));

				if(stringValue == null || field.getType() == String.class)
				{
					field.set(instance, stringValue);
				}
				else if(field.getType() == Long.class)
				{
					field.set(instance, Long.parseLong(stringValue));
				}
				else if(field.getType() == Boolean.class)
				{
					field.set(instance, Boolean.parseBoolean(stringValue));
				}
				else if(field.getType() == Double.class)
				{
					field.set(instance, Double.parseDouble(stringValue));
				}
				else
				{
					String exceptionMessage = String.format(PARSE_MISSING_MESSAGE, field.getName(), tableName, stringValue);
					throw new Exception(exceptionMessage);
				}
			}
		}

		return (T) instance;
	}


	private List<T> createListFromCursor(Cursor cursor) throws Exception
	{
		List<T> resultsList = new ArrayList<T>(cursor.getCount());

		if(cursor.moveToFirst())
		{
			T domainObject = createOneFromCursor(cursor);
			resultsList.add(domainObject);

			while(cursor.moveToNext())
			{
				domainObject = createOneFromCursor(cursor);
				resultsList.add(domainObject);
			}

		}

		return resultsList;
	}


	public void create() throws Exception
	{
		ContentValues values = getContentValues();
		this._id = DatabaseManager.getInstance().getDatabase().insert(tableName, null, values);
	}


	public void delete() throws Exception
	{
		DatabaseManager.getInstance().getDatabase().delete(tableName, "_id=" + _id, null);
	}


	public void deleteAll() throws Exception
	{
		DatabaseManager.getInstance().getDatabase().delete(tableName, null, null);
	}


	public void update() throws Exception
	{
		DatabaseManager.getInstance().getDatabase().update(tableName, getContentValues(), "_id=" + _id, null);
	}


	public List<T> listAll() throws Exception
	{
		Cursor cursor = DatabaseManager.getInstance().getDatabase().query(tableName, new String[]{"*"}, null, null, null, null, null);
		List<T> objectList = new ArrayList<T>();

		if(cursor != null)
		{
			cursor.moveToFirst();

			while(!cursor.isAfterLast())
			{
				objectList.add(createOneFromCursor(cursor));
				cursor.moveToNext();
			}

			cursor.close();
		}

		return objectList;
	}

	@JsonIgnore
	public T get(Long rowId) throws Exception
	{
		if(rowId == null){
			throw new FactoidException("A aplicação tentou encontrar um registro com id nulo na tabela " + tableName + ".");
		}

		T domainObject = null;
		Cursor cursor = DatabaseManager.getInstance().getDatabase().query(true, tableName, new String[]{"*"}, "_id=" + rowId, null, null, null, null, null);
		if(cursor == null || !cursor.moveToFirst())
		{
			throw new FactoidException("A aplicação não encontrou o registro com id " + String.valueOf(rowId) + " na tabela " + tableName + ".");
		}
		
		domainObject = createOneFromCursor(cursor);
		cursor.close();

		return domainObject;
	}

	@JsonIgnore
	public T getOne(String query, String[] queryParams) throws Exception
	{
		Cursor cursor = DatabaseManager.getInstance().getDatabase().query(false, tableName, new String[]{"*"}, query, queryParams, null, null, null, null);
		T domainObject = null;
		if(cursor != null)
		{
			cursor.moveToFirst();
			if(!cursor.isAfterLast())
				domainObject = createOneFromCursor(cursor);
		}

		cursor.close();

		return domainObject;
	}

	@JsonIgnore
	public List<T> getList(String query, String param) throws Exception
	{
		return getList(query, new String[]{param});
	}

	@JsonIgnore
	public List<T> getList(String query, String[] queryParams) throws Exception
	{
		Cursor cursor = DatabaseManager.getInstance().getDatabase().query(false, tableName, new String[]{"*"}, query, queryParams, null, null, null, null);

		List<T> resultsList = createListFromCursor(cursor);
		cursor.close();

		return resultsList;
	}

	@JsonIgnore
	public List<T> getSortedList(String query, String[] queryParams, String orderByClause) throws Exception
	{
		Cursor cursor = DatabaseManager.getInstance().getDatabase().query(false, tableName, new String[]{"*"}, query, queryParams, null, null, orderByClause, null);

		List<T> resultsList = createListFromCursor(cursor);
		cursor.close();

		return resultsList;
	}


	public List<T> queryListFromCursor(Cursor cursor) throws Exception
	{
		List<T> resultsList = createListFromCursor(cursor);
		cursor.close();

		return resultsList;
	}


	public <U extends AnnotatedTable<T>> void deleteList(List<U> domainList) throws Exception
	{
		for(U domain : domainList)
		{
			domain.delete();
		}
	}

	@JsonIgnore
	public int getCount(String query, String[] queryParams)
	{
		return getCount(tableName, query, queryParams);
	}
	public void createOrUpdate() throws Exception{
		if (this._id == null)
			create();
		else
			update();
	}
	@JsonIgnore
	public int getCount(String pTableName, String query, String[] queryParams)
	{
		int resultCount = 0;
		Cursor resultCursor = DatabaseManager.getInstance().getDatabase().query(false, pTableName, new String[]{"count(_id)"}, query, queryParams, null, null, null, null);
		resultCursor.moveToFirst();

		try
		{
			resultCount = resultCursor.getInt(0);
		}
		catch(Exception ex)
		{

		}
		finally
		{
			resultCursor.close();
		}

		return resultCount;
	}
}
