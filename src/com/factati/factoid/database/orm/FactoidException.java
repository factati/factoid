package com.factati.factoid.database.orm;

@SuppressWarnings("serial")
public class FactoidException extends Exception {

	public FactoidException(String message) {
		super(message);
	}

}
