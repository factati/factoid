package com.factati.factoid.database.orm;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreType;

@JsonIgnoreType
public interface IDatabaseModel {
	@JsonIgnore
	public String getCreationScript();
	
	@JsonIgnore
	public String getDropScript();
}
