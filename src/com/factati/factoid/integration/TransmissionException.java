package com.factati.factoid.integration;

import java.io.IOException;

@SuppressWarnings("serial")
public class TransmissionException extends IOException {

	public TransmissionException(String message){
		super(message);
	}
	
	public String toString(){
		return this.getMessage();
	}
}
